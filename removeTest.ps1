if (Test-Path ./gino_fastapi) {
    Remove-Item -Recurse -Force -ErrorAction Ignore ./gino_fastapi
}

if (Test-Path ./cookiecutter-debug) {
    Remove-Item -Recurse -Force -ErrorAction Ignore gino_fastapi
}
