import pytest
from {{cookiecutter.python_slug}}.utils.login import verify_password, get_password_hash

@pytest.mark.parametrize('pass_in', [
    'asdf',
    'asdf1234',
    'My Name Is Mike',
    'I L1k3 t0 P@rty!',
])
def test_get_password_hash(pass_in):
    hashed = get_password_hash(pass_in)
    assert verify_password(pass_in, hashed)
