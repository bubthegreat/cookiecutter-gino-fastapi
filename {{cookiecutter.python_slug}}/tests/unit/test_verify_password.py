import pytest
from {{cookiecutter.python_slug}}.utils.login import verify_password


@pytest.mark.parametrize('pass_in,hashed_password', [
    ('asdf', '$2b$12$.NZyGXtFmP.yoOYV2/oikONozeRNG51LcSFesX1cSigXGZW9DqDWK'),
    ('asdf1234', '$2b$12$WwAEFUw4Ng/EO.Dpw6PS4.nuCHsYO2AmBuMxAQ54R0A13GxuSYWOm'),
    ('My Name Is Mike', '$2b$12$kXHXwSiDE3ySqAyVqZXLQONtjKqZFnImPVJ3So068b5iWUV2s0NSe'),
    ('I L1k3 t0 P@rty!', '$2b$12$.MV/3bnhmmzaSbeodLOpUOBIURx.XevYY0V7l95ErW2BdMc34bhxm'),
])
def test_verify_password(pass_in, hashed_password):
    assert verify_password(pass_in, hashed_password)

@pytest.mark.parametrize('pass_in,hashed_password', [
    ('asdf', '$2b$12$.MV/3bnhmmzaSbeodLOpUOBIURx.XevYY0V7l95ErW2BdMc34bhxm'),
    ('asdf1234', '$2b$12$kXHXwSiDE3ySqAyVqZXLQONtjKqZFnImPVJ3So068b5iWUV2s0NSe'),
    ('My Name Is Mike', '$2b$12$WwAEFUw4Ng/EO.Dpw6PS4.nuCHsYO2AmBuMxAQ54R0A13GxuSYWOm'),
    ('I L1k3 t0 P@rty!', '$2b$12$.NZyGXtFmP.yoOYV2/oikONozeRNG51LcSFesX1cSigXGZW9DqDWK'),
])
def test_verify_bad(pass_in, hashed_password):
    assert not verify_password(pass_in, hashed_password)
