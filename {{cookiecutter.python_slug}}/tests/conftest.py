import asyncio
import requests
import pytest
from alembic.config import main
from starlette.testclient import TestClient
from {{cookiecutter.python_slug}} import config
from {{cookiecutter.python_slug}}.models import db
from {{cookiecutter.python_slug}}.main import get_app

import pathlib
import pytest


def pytest_collection_modifyitems(config, items):
    """Mark subfolders in the tests directory for ease of use.

    This will mark everything under the test directory as a pytest.mark if the mark
    is defined in the pytest.ini.  Currently only the parent level directories are
    defined, so the markers that will be added dynamically are e2e, integration, and
    unit.  Markers may still be added explicitly.


    """
    rootdir = pathlib.Path(config.rootdir)
    for item in items:
        rel_path = pathlib.Path(item.fspath).relative_to(rootdir)
        mark_name = rel_path.parts[0] if rel_path.parts[0] else None
        for part in rel_path.parts[1:]:
            # Don't mark tests as their own marker.
            if part.endswith('.py') or part.startswith('test_'):
                continue
            mark = getattr(pytest.mark, part, None)
            # If the marker isn't defined in the pytest.ini, also don't mark them.
            if not mark:
                continue
            item.add_marker(mark)



@pytest.fixture(scope="session")
async def pg_db():

    main(["--raiseerr", "upgrade", "head"])

    await db.set_bind(f'postgresql://{config.DB_USER}:{config.DB_PASSWORD}@{config.DB_HOST}:{config.DB_PORT}/{config.DB_DATABASE}')
    # await db.set_bind("sqlite:///./test.db")
    await db.gino.create_all()
    yield db

    main(["--raiseerr", "downgrade", "base"])


@pytest.fixture(scope="session")
def app_routes():
    app = get_app()
    all_routes = {}
    for route in app.routes:
        if hasattr(route, 'tags'):
            route_tag = route.tags[0]
            if not all_routes.get(route_tag):
                all_routes[route_tag] = []
            all_routes[route_tag].append(route.name)
    return all_routes


@pytest.fixture(scope="session")
def superuser_data():

    first_superuser_data = {
    "username": "string",
    "password": "string",
    "password_repeat": "string",
    "first_name": "string",
    "last_name": "string"
    }
    return first_superuser_data


@pytest.fixture(scope="session")
def session(superuser_data):
    response = requests.post('http://localhost:8000/su/usermod', json=superuser_data)
    login_data = {
        "username": superuser_data["username"],
        "password": superuser_data["password"],
    }
    s = requests.Session()
    response = s.post('http://localhost:8000/token/', data=login_data)
    tokens = response.json()
    a_token = tokens["access_token"]
    headers = {"Authorization": f"Bearer {a_token}"}
    s.headers.update(headers)
    yield s


@pytest.yield_fixture(scope='session')
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()
