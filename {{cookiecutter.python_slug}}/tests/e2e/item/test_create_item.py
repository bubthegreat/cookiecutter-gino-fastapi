import pytest


def test_create_item(session, pg_db) -> None:
    data = {
        "name": "test_item", "description": "Test Item"
    }
    response = session.post(
        "http://localhost:8000/items/", json=data,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["name"] == data["name"]
    assert content["description"] == data["description"]
    assert "id" in content
