import pytest
import requests

def test_get_access_token(superuser_data) -> None:
    login_data = {
        "username": superuser_data["username"],
        "password": superuser_data["password"],
    }
    response = requests.post('http://localhost:8000/token/', data=login_data)
    tokens = response.json()

    assert response.status_code == 200
    assert "access_token" in tokens
    assert tokens["access_token"]
