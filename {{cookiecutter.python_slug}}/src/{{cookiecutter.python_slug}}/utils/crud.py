"""Basic autocrud utils."""

import logging
from typing import List

from fastapi import FastAPI, APIRouter, Depends

from {{cookiecutter.python_slug}}.models.users import User
from {{cookiecutter.python_slug}}.models import db
from {{cookiecutter.python_slug}}.utils.login import get_current_active_user, get_active_superuser


LOGGER = logging.getLogger(__name__)


def add_crud_router(app: FastAPI, model: db.Model) -> None:
    """Add a crud router for the model."""

    LOGGER.info("Loading model %s.", model)

    router = APIRouter()

    validator = model.get_validator()
    post_validator = (
        model.get_validator(method="post") if hasattr(model, "__post_override__") else validator
    )
    patch_validator = (
        model.get_validator(method="patch")
        if hasattr(model, "__patch_override__")
        else validator
    )

    @router.get("/{uid}", response_model=validator)
    async def get_instance(  # pylint: disable=unused-variable
        uid: int,
        current_user: User = Depends(get_current_active_user)  # pylint: disable=unused-argument
    ) -> db.Model:
        """Get instance of model."""
        LOGGER.info("Retrieving %s for uid %s", model.__name__, uid)
        instance = await model.get_or_404(uid)
        LOGGER.info("Retrieved %s for uid %s", model.__name__, uid)
        cleaned = validator(**instance.to_dict())
        return cleaned

    @router.get("/all/", response_model=List[validator])  # type: ignore
    async def get_all(  # pylint: disable=unused-variable
        current_user: User = Depends(get_current_active_user)  # pylint: disable=unused-argument
    ) -> List[db.Model]:
        """Get all instances of model."""
        LOGGER.info("Retrieving all instances for %s", model.__name__)
        raw_results = await model.query.gino.all()
        results = [validator(**instance.to_dict()) for instance in raw_results]
        return results

    @router.post("/")
    async def add_instance(  # pylint: disable=unused-variable
        instance: post_validator,  # type: ignore
        current_user: User = Depends(get_active_superuser)  # pylint: disable=unused-argument
    ) -> db.Model:
        """Add an instance of the model."""
        LOGGER.info("Creating %s for %s", model.__name__, instance)
        created = await model.create(**instance.dict())  # type: ignore
        cleaned = validator(**created.to_dict())
        LOGGER.info("Created %s: id=%s", model.__name__, created.id)
        return cleaned

    @router.delete("/{uid}")
    async def delete_instance(  # pylint: disable=unused-variable
        uid: int,
        current_user: User = Depends(get_current_active_user)  # pylint: disable=unused-argument
    ) -> bool:
        """Remove an instance of the model."""
        LOGGER.info("Deleting %s for id %s", model.__name__, uid)
        instance = await model.get_or_404(uid)
        await instance.delete()
        LOGGER.info("Deleted %s for id %s", model.__name__, uid)
        return True

    @router.patch("/{uid}", response_model=validator)
    async def update_instance(  # pylint: disable=unused-variable
        uid: int,
        patch_data: patch_validator,  # type: ignore
        current_user: User = Depends(get_current_active_user),  # pylint: disable=unused-argument
    ) -> db.Model:
        """Update an instance of the model."""
        instance = await model.get_or_404(uid)
        await instance.update(**patch_data.dict()).apply()  # type: ignore
        cleaned = patch_validator(**instance.to_dict())
        return cleaned

    app.include_router(
        router,
        prefix=f"/{model.get_prefix()}",
        tags=model.get_tags(),
        responses={404: {"description": "Not found"}},
    )


def add_model_crud(app: FastAPI, models: List[db.Model]) -> None:
    """Add model crud routes for models."""
    for model in models:
        add_crud_router(app, model)
        LOGGER.info("Added CRUD for %s.", model.__name__)
    LOGGER.info("Finished adding CRUD for %s models.", len(models))
