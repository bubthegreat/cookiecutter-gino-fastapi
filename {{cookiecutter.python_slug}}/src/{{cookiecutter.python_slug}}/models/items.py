"""An example items model."""

from {{cookiecutter.python_slug}}.models import db, CRUDBase


class Item(db.Model, CRUDBase):
    """Some random items for your database!"""

    __tablename__ = "items"
    __post_override__ = {
        "exclude": ["id"]
    }

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(), nullable=False)
    description = db.Column(db.Unicode(), nullable=False)
