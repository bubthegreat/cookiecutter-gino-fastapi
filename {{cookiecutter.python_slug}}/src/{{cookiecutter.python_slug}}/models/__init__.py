"""Base classes for all CRUD models and DB configs."""

import logging
import types
from typing import Dict, List, Optional, Type

from gino.ext.starlette import Gino  # pylint: disable=no-name-in-module,import-error
from pydantic import BaseModel, create_model  # pylint: disable=no-name-in-module

from {{cookiecutter.python_slug}} import config

LOGGER = logging.getLogger(__name__)


db = Gino(
    dsn=config.DB_DSN,
    pool_min_size=config.DB_POOL_MIN_SIZE,
    pool_max_size=config.DB_POOL_MAX_SIZE,
    echo=config.DB_ECHO,
    ssl=config.DB_SSL,
    use_connection_for_request=config.DB_USE_CONNECTION_FOR_REQUEST,
    retry_limit=config.DB_RETRY_LIMIT,
    retry_interval=config.DB_RETRY_INTERVAL,
)
LOGGER.info("Database instance created: %s", db)


class CRUDBase:
    """Base CRUD object to build autocrud models off of."""

    __tablename__: str

    @classmethod
    def get_table(cls) -> str:
        """Get the table of the CRUD model."""
        if not hasattr(cls, "__tablename__"):
            raise ValueError(f"__tablename__ must be defined on model {cls.__name__}", )
        return str(getattr(cls, "__tablename__"))

    @classmethod
    def get_prefix(cls) -> str:
        """Return the prefix used for the model."""
        return cls.get_table()

    @classmethod
    def get_tags(cls) -> List[str]:
        """Return a default tags list for the model."""
        return [cls.get_table().title()]

    @classmethod
    def _get_override_keys(
        cls,
        method: Optional[str],
        valid_keys: List[str],
        override_dict: Dict[str, str]
    ) -> List[str]:
        """Get override keys when they're set for a method."""
        method_dunder = f"__{method}_override__"
        if not hasattr(cls, method_dunder):
            LOGGER.info(
                "No overrides found for method: '%s' - returning valid_keys", method
            )
            return valid_keys

        LOGGER.info("%s method validator.", method)
        overrides = getattr(cls, method_dunder)
        additional_excluded = overrides.get("exclude", [])
        LOGGER.info("Additional excluded: %s", additional_excluded)
        valid_keys = [key for key in valid_keys if key not in additional_excluded]
        override = overrides.get("override", {})
        additional_added = overrides.get("additional", [])
        for additional_key in additional_added:
            valid_keys.append(additional_key)
        for key, val in override.items():
            override_dict[key] = val
            valid_keys.append(key)
        LOGGER.info("override_dict: %s", override_dict)
        return valid_keys

    @classmethod
    def get_validator(cls, method: str = None) -> Type[BaseModel]:
        """Get a validator for the given method type for this model."""
        method_dunder = f"__{method}_override__" if method else None
        override_dict: Dict[str, str] = {}
        additional_added = {}

        LOGGER.info("Getting initial keys for %s", cls.__name__)
        keys = [str(key) for key in cls.__dict__]
        if method_dunder:
            overrides = getattr(cls, method_dunder)
            additional_added = overrides.get("additional", {})
            if additional_added:
                LOGGER.info("Found additional keys to add: %s", additional_added)
            for additional_key in additional_added:
                keys.append(additional_key)
                LOGGER.info("Added additional key: %s", additional_key)
        valid_keys = [key for key in keys if not key.startswith("_")]
        LOGGER.info("Valid keys without exclusions: %s", valid_keys)

        if hasattr(cls, "__excluded__"):
            LOGGER.info("Found excluded")
            excl_keys = getattr(cls, "__excluded__")
            LOGGER.info("Excluded keys: %s", excl_keys)
            valid_keys = [key for key in valid_keys if key not in excl_keys]
            LOGGER.info("Valid keys with exclusions: %s", valid_keys)

        valid_keys = cls._get_override_keys(method, valid_keys, override_dict)
        LOGGER.info("Valid keys without overrides: %s", valid_keys)

        field_definitions = {}
        for key in valid_keys:
            col = getattr(cls, key, None)
            if override_dict.get(key):
                key = override_dict[key]
            if key in additional_added:
                field_definitions[key] = (additional_added[key], ...)
            if col is None:
                continue
            if isinstance(col, (types.MethodType, types.FunctionType)):
                # Don't snag on properties or methods.
                continue
            LOGGER.debug("Getting validator attribute for %s", key)
            field_definitions[key] = (col.type.python_type, ...)
        validator_name = f"{cls.__name__}{'.' + method if method else ''}"
        LOGGER.info(
            "Creating validator %s with keys: %s",
            validator_name,
            list(field_definitions.keys()),
        )
        # Ignoring the typing here because it isn't well defined.
        validator = create_model(validator_name, **field_definitions)  # type: ignore

        return validator
