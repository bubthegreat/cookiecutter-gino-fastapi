"""Import the application for use in asgi."""

from {{cookiecutter.python_slug}}.main import get_app

app = get_app()
