"""Main instantiation of the API."""

import json
import logging

from fastapi import FastAPI

import uvicorn

from {{cookiecutter.python_slug}}.models import db
from {{cookiecutter.python_slug}}.models.items import Item
from {{cookiecutter.python_slug}}.utils.crud import add_model_crud
from {{cookiecutter.python_slug}}.routes.users import add_users_router
from {{cookiecutter.python_slug}}.routes.superusers import add_superusers_router
from {{cookiecutter.python_slug}}.routes.token import add_token_router

LOGGER = logging.getLogger(__name__)

# Add autocrud models here
CRUD_MODELS = [
    Item,
]

# Add special routers here.
ROUTERS = [
    add_token_router,
    add_users_router,
    add_superusers_router,
]


def get_app() -> FastAPI:
    """Get an app instance."""
    app = FastAPI(title="This is a basic GINO FastAPI implementation!")
    LOGGER.info("Initializing db.")
    db.init_app(app)
    LOGGER.info("Initialization of db complete.")

    # Main CRUD routers added.
    LOGGER.info("Adding CRUD routers for %s models", len(CRUD_MODELS))
    add_model_crud(app, CRUD_MODELS)

    LOGGER.info("Adding special routers.")
    for add_router_func in ROUTERS:
        add_router_func(app)
        LOGGER.info("Added special router %s", add_router_func.__name__)

    return app

def get_openapi_json() -> None:
    """Save the openapi.json file in the current directory."""
    LOGGER.info("Creating openapi.json")
    app = get_app()
    spec = app.openapi()
    with open('openapi.json', 'w') as filehandle:
        json.dump(spec, filehandle)
    LOGGER.info("Created openapi.json")


def run() -> None:
    """Run through uvicorn when run."""
    uvicorn.run(
        "gino_fastapi.asgi:app",
        host="localhost",
        port=8000,
        reload=True,
        log_level="trace",
    )
