"""Basic user utils."""

import logging
from typing import List

from fastapi import FastAPI, APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer

from {{cookiecutter.python_slug}}.models.users import User
from {{cookiecutter.python_slug}}.validators import PatchUser
from {{cookiecutter.python_slug}}.models import db
from {{cookiecutter.python_slug}}.utils.login import get_current_active_user, get_active_superuser


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

LOGGER = logging.getLogger(__name__)


def add_users_router(app: FastAPI) -> None:
    """Add user routes to the app."""

    LOGGER.info("Loading user routes.")

    router = APIRouter()

    validator = User.get_validator()
    post_validator = User.get_validator(method="post")
    patch_validator = User.get_validator(method="patch")

    @router.get("/{uid}", response_model=validator)
    async def get_instance(  # pylint: disable=unused-variable
        uid: int, current_user: User = Depends(get_current_active_user)
    ) -> db.Model:
        """Get a single user instance."""
        if current_user.id != uid:
            # User creation is special, because it includes permissions
            # and access tokens, superuser, etc.
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot get user information about others unless you're a superuser."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")
        LOGGER.info("Retrieving User for uid %s", uid)
        instance = await User.get_or_404(uid)
        LOGGER.info("Retrieved User for uid %s", uid)
        cleaned = validator(**instance.to_dict())
        return cleaned

    @router.get("/all/", response_model=List[validator])  # type: ignore
    async def get_all(  # pylint: disable=unused-variable
        current_user: User = Depends(get_current_active_user)
        ) -> List[db.Model]:
        """Get all user instances."""
        if not current_user.is_superuser and current_user.is_active:
            LOGGER.info("Cannot list all users unless you're a super user and active.")
            raise HTTPException(status_code=403, detail="Unauthorized")
        LOGGER.info("Retrieving all instances for User")
        raw_results = await User.query.gino.all()
        results = [validator(**instance.to_dict()) for instance in raw_results]
        return results

    @router.post("/")
    async def add_instance(  # pylint: disable=unused-variable
        instance: post_validator,  # type: ignore
        current_user: User = Depends(get_active_superuser)
    ) -> db.Model:
        """Add a user instance."""
        LOGGER.info("Creating User for %s", instance)
        if not current_user.is_superuser and current_user.is_active:
            LOGGER.info(
                "Cannot create another user unless you're a super user and active."
            )
            raise HTTPException(status_code=403, detail="Unauthorized")
        created = await create_user(instance)
        cur_validator = User.get_validator()
        cleaned = cur_validator(**created.to_dict())
        LOGGER.info("Created User: id=%s", created.id)
        return cleaned

    @router.delete("/{uid}")
    async def delete_instance(  # pylint: disable=unused-variable
        uid: int, current_user: User = Depends(get_current_active_user)
    ) -> bool:
        """Delete a user instance."""
        if current_user.id != uid:
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot delete another user unless you're a super user and active."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")

        LOGGER.info("Deleting User for id %s", uid)
        instance = await User.get_or_404(uid)
        await instance.delete()
        LOGGER.info("Deleted User for id %s", uid)
        return True

    @router.patch("/{uid}", response_model=validator)
    async def update_instance(  # pylint: disable=unused-variable
        uid: int,
        patch_data: patch_validator,  # type: ignore
        current_user: User = Depends(get_current_active_user),
    ) -> db.Model:
        """Update a user instance."""
        if current_user.id != uid:
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot modify another user unless you're a super user and active."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")
        instance = await User.get_or_404(uid)
        await instance.update(**patch_data.dict()).apply()  # type: ignore
        cleaned = patch_validator(**instance.to_dict())
        return cleaned

    app.include_router(
        router,
        prefix="/user",
        tags=["User"],
        responses={404: {"description": "Not found"}},
    )

    @app.get("/user/current/", tags=["User"])
    async def read_current_user(  # pylint: disable=unused-variable
        current_user: User = Depends(get_current_active_user)
        ) -> PatchUser:
        """Read the current user that's logged in."""
        LOGGER.info("Ahead")
        cleaned = PatchUser(**current_user.to_dict())
        LOGGER.info("After")
        return cleaned



async def create_user(
    new_user: User.get_validator()  # type: ignore
    ) -> db.Model:
    """Create a user instance."""
    nudict = new_user.dict()
    password = nudict.pop("password")
    password_repeat = nudict.pop("password_repeat")
    if password != password_repeat:
        raise HTTPException(status_code=403, detail="Mismatched passwords")
    hashed_password = User.hash_password(password)
    nudict["hashed_password"] = hashed_password
    existing_users = len(await db.all(User.query))
    LOGGER.info("Number of existing users: %s", existing_users)
    if not existing_users:
        LOGGER.info("No existing users, creating superuser")
        nudict["is_superuser"] = True
        LOGGER.info("No existing users, marking as active")
        nudict["is_active"] = True

    user = await User.create(**nudict)

    return user
