"""Basic superuser utils."""

import logging
from typing import Dict, Union

from fastapi import FastAPI, APIRouter, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from {{cookiecutter.python_slug}}.utils.login import get_access_token

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

LOGGER = logging.getLogger(__name__)


def add_token_router(app: FastAPI) -> None:
    """Add router for token API requests."""

    LOGGER.info("Loading token routes.")

    router = APIRouter()

    @router.post("/")
    async def get_token(  # pylint: disable=unused-variable
        form_data: OAuth2PasswordRequestForm = Depends()
        ) -> Dict[str, Union[str, bytes]]:
        LOGGER.info("Attempting to get token data: %s", form_data)
        token = await get_access_token(form_data)
        LOGGER.info("Got token: %s", token)
        return token

    app.include_router(
        router,
        prefix="/token",
        tags=["Token"],
        responses={404: {"description": "Not found"}},
    )
