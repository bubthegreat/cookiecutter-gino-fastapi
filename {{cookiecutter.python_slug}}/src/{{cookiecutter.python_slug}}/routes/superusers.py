"""Basic superuser utils."""

import logging

from fastapi import FastAPI, APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer

from {{cookiecutter.python_slug}}.models.users import User
from {{cookiecutter.python_slug}}.models import db
from {{cookiecutter.python_slug}}.validators import PatchUser, NewUser
from {{cookiecutter.python_slug}}.utils.login import get_active_superuser
from {{cookiecutter.python_slug}}.routes.users import create_user



oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


LOGGER = logging.getLogger(__name__)


def add_superusers_router(app: FastAPI) -> None:
    """Add superuser router to the FastAPI app."""

    LOGGER.info("Loading superuser routes.")

    router = APIRouter()

    @router.patch("/usermod", response_model=PatchUser)
    async def modify_existing_user(  # pylint: disable=unused-variable
        uid: int,
        patch_data: PatchUser,
        current_user: User = Depends(get_active_superuser),  # pylint: disable=unused-argument
    ) -> PatchUser:
        """Add patch endpoint for superusers to edit users."""
        instance = await User.get_or_404(uid)
        existing_user = await User.query.where(
            User.username == patch_data.username
        ).gino.first()
        LOGGER.info("Existing user: %s", existing_user)
        if existing_user and patch_data.username != instance.username:
            LOGGER.info(
                "Cannot change username from %s to %s - the username is already in use.",
                patch_data.username,
                existing_user.username,
            )
            raise HTTPException(status_code=403, detail="Username already exists.")
        await instance.update(**patch_data.dict()).apply()
        cleaned = PatchUser(**instance.to_dict())
        return cleaned

    @router.post("/usermod",)
    async def add_initial_superuser(  # pylint: disable=unused-variable
        instance: NewUser
        ) -> db.Model:
        """Add endpoint to add initial superuser."""
        existing_users = len(await db.all(User.query))
        if existing_users:
            raise HTTPException(status_code=403, detail="Unauthorized")
        created = await create_user(instance)
        cur_validator = User.get_validator()
        cleaned = cur_validator(**created.to_dict())
        LOGGER.info("Created SuperUser: id=%s", created.id)
        return cleaned

    app.include_router(
        router,
        prefix="/su",
        tags=["Superuser"],
        responses={404: {"description": "Not found"}},
    )
