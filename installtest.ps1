if (Test-Path .\gino_fastapi) {
    Remove-Item -Recurse -Force -ErrorAction Ignore .\gino_fastapi
}

if (Test-Path .\cookiecutter-debug) {
    Remove-Item -Recurse -Force -ErrorAction Ignore .\cookiecutter-debug
}

cookiecutter --no-input --debug-file .\cookiecutter-debug .
./venv/Scripts/Activate.ps1
Set-Location .\gino_fastapi\
pip install -r requirements/development.txt
python setup.py sdist
pip install ./dist/gino_fastapi-0.0.0a0.tar.gz
docker-compose down -v
Invoke-Expression "&'./setEnv.ps1'"
docker-compose up -d
Start-Sleep -s 3
Write-Output "Creating database migration"
alembic revision --autogenerate -m 'Initial Migration'
Write-Output "Running database migrations"
alembic upgrade head
Write-Output "Starting app."
uvicorn gino_fastapi.asgi:app --reload
