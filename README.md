# GINO FastAPI

Before you consider using this cookiecutter project, please understand when it's useful and when it's not!

https://python-gino.org/docs/en/master/explanation/why.html

The tutorial for GINO walk through a FastAPI server - this is a cookiecutter version of that that expands on the tutorial to include token auth and basic user CRUD operations, sample migrations, compose files for deploying immediately and testing locally, and utility scripts for contributing to the cookiecutter in windows.


GINO FastAPI Tutorial: https://python-gino.org/docs/en/master/tutorials/fastapi.html

# Getting Started

1. Clone Repo
2. cookiecutter --no-input --debug-file .\cookiecutter-debug .
3. poetry install
4. docker-compose up -d
5. Set environment variables
6. poetry run alembic revision --autogenerate -m 'Initial Migration'
7. poetry run alembic upgrade head
8. poetry run uvicorn gino_fastapi.asgi:app --reload

Initial environment variables required if you're not using powershell:

```
DB_DRIVER = "postgresql"
DB_HOST = "localhost"
DB_PORT = "5432"
DB_USER = "postgresql"
DB_PASSWORD = "postgresql"
DB_RETRY_LIMIT = 10
DB_RETRY_INTERVAL = 5
DB_DATABASE = "gino_fastapi_db"
PYTHONDONTWRITEBYTECODE = 1
```



# TODO:
* Move settings into cookiecutter template for env vars
* Add bash scripts for quick start
* Add unit tests
* Add integration tests
* Deploy an initial example somewhere
* Add compose for deployment
